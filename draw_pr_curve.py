import matplotlib.pyplot as plt


x=[]
y=[]


with open("result.txt") as f:
    content = f.readlines()
    for line in content:
        fields = line.split(" ")

        if (fields[0] == "Threshold"):
            continue
        elif(fields[1] == "for"):
            precision = float(fields[7].split(",")[0])
            recall = float(fields[10].split(",")[0])
            print("Precision and recall: " + str(precision) + " " + str(recall))
            x.append(recall)
            y.append(precision)
        else:
            mAP = float(fields[6].split(",")[0])
            print("mAP " + str(mAP))

plt.plot(x, y, 'ro')
plt.ylabel("precision")
plt.xlabel("recall")
plt.show()
