# Python script to change Kitti label format to Yolo format
#
# KITTI FIELDS (each line):
# [0]           1 type Describes the type of object: 'Car', 'Van', ...
# [1]           1 truncated Float from 0 (non-truncated) to 1 (truncated), where truncated refers to the object leaving image boundaries
# [2]           1 occluded Integer (0,1,2,3) indicating occlusion state: 0 = fully visible, 1 = partly occluded, 2 = largely occluded, 3 = unknown
# [3]           1 alpha Observation angle of object, ranging [-pi..pi]
# [4][5][6][7]  4 bbox 2D bounding box of object in the image (0-based index):
#               contains left, top, right, bottom pixel coordinates
# [8][9][10]    3 dimensions 3D object dimensions: height, width, length (in meters)
# [11][12][13]  3 location 3D object location x,y,z in camera coordinates (in meters)
# [14]          1 rotation_y Rotation ry around Y-axis in camera coordinates [-pi..pi]
# [15]          1 score Only for results: Float, indicating confidence in detection, needed for p/r curves, higher is better.


###### How to use: ##################################################
#   Place this script in a folder containing IMAGES and LABELS.     #
#   IMAGES = KITTI images folder                                    #
#   LABELS = KITTI labels folder                                    #
#####################################################################

#TODO: separate into different functions

import cv2
import sys

entry_index = 0

if len(sys.argv) != 1:
    print("No arguments needed")
    exit()

#out_file_name = sys.argv[1]

### configuration ###

images_path = "image_2\\"
labels_path = "label_2\\"
image_format = ".png"

file_list_name = "names.txt"    # it contains the absolute path of each image (use "script_names" to generate it)
file_list = open(file_list_name, "r")



# create output file that will contain the entries to train darknet YOLO network
# entry format:     # format:   object label box_1
#                               object_label box_2
#                                  ....
#                               object_label box_n
#
#                               <box_x> := x y width height
#                               <x, y>  := center of the box

for file_path in file_list:
    print("\n ###################### Reading file " + file_path + "######################")

    file_path = file_path.split("\n")[0] # erase the "\n" special character
    filename = file_path.split(".")[0].split("\\")[-1] # takes the last field
    out_label = open("out\\" + filename + ".txt", "w+")

    print("DEBUG: filename " + filename)

    label_file = open(labels_path + filename + ".txt", "r")

    ### Reading corresponding image ###
    #TODO
    img = cv2.imread(images_path + filename + image_format)

    # retreive image shape
    img_height, img_width, _ = img.shape
    print("DEBUG: img_height:" + str(img_height) + " img_width: " + str(img_width))
    ###################################


    # preparing resulting string
    # format:   object label box_1
    #           object_label box_2
    #           ....
    #           object_label box_n
    #
    # <box_x> := x y width height
    # <x, y>  := center of the box
    result_string = ""

    # For each line, append the bbox informations [label left top right bottom]
    for line in label_file:
        print("___________")
        print("\nParsing the following line: \n" + line)

        parameters = line.split(" ")
        print("Debug: " + str(parameters))
        object_type = parameters[0]
        bbox_left = float(parameters[4])
        bbox_top = float(parameters[5])
        bbox_right = float(parameters[6])
        bbox_bottom = float(parameters[7])

        # converting to darknet YOLO label tipe
        absolute_image_width = bbox_right - bbox_left
        absolute_image_height = bbox_bottom - bbox_top
        absolute_image_x = (absolute_image_width/2) + bbox_left
        absolute_image_y = (absolute_image_height/2) + bbox_top

        # normalization step
        image_width = absolute_image_width / img_width
        image_height = absolute_image_height / img_height
        image_x = absolute_image_x / img_width
        image_y = absolute_image_y / img_height
        print("Debug: image_width: " + str(image_width) + " image_height: " + str(image_height) + " image_x: " + str(image_x) + " image_y: " + str(image_y))

        # For now the script will only manage 3 types of objects!
        if object_type == 'Pedestrian':
            print("DEBUG: " + object_type + " -> label 0")
            object_label = 0
        elif object_type == 'Car':
            print("DEBUG: " + object_type + " -> label 1")
            object_label = 1
        elif object_type == 'Cyclist':
            print("DEBUG: " + object_type + " -> label 2")
            object_label = 2
        elif object_type == 'Van':
            print("DEBUG: " + object_type + " -> label 3")
            object_label = 3
        elif object_type == 'Truck':
            print("DEBUG: " + object_type + " -> label 4")
            object_label = 4
        elif object_type == 'Tram':
            print("DEBUG: " + object_type + " -> label 5")
            object_label = 5
        elif object_type == 'DontCare':
            print("DEBUG: " + object_type + " -> label 6")
            object_label = 6
        else:
            print("DEBUG: ignoring entry \"" + object_type + "\"...")
            object_label = -1
            continue # SKIP TO NEXT ITERATION

        # build the string to be written ## TO BE CHECKED
        obj_string = str(object_label) + " " + str(image_x) + " " + str(image_y) + " " + str(image_width) + " " + str(image_height) + "\n"
        print("DEBUG: object string: " + obj_string)
        result_string += obj_string


    label_file.close()
    print("DEBUG: ####### Final string: " + result_string)

    # print the result_string to already opened file
    out_label.write(result_string)
    out_label.close()

    # increase the entry value
    entry_index+=1

#out_f.close()

# To read a file
#f = open("labelX.txt", "a")
#f.write("Now the file has more content!")
#f.close()
