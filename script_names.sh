# redirect output to file with >> names.txt
if [ "$#" -ne 1 ]; then
    	echo "Illegal number of parameters"
    	exit
fi

FILE_PATH=$PWD"/"$1
cd $FILE_PATH

for file in $FILE_PATH/* ; 
	do 
	echo "$(realpath $file)"; 
done
cd ..
