import matplotlib.pyplot as plt


result_string = "\\begin{table}[ht]\n\\begin{center}\n\\begin{tabular}{ c | c | c | c | c | c | c | c }"
result_string = "%s\n Class name & AP & TP & FP & FN & IoU & precision & recall \\\\ \n\\hline" % result_string

with open("table.txt") as f:
    content = f.readlines()
    for line in content:
        line = line.replace(" ", "")
        fields = line.split(",")
        #print("First field: " + fields[0])
        if (fields[0].split(",")[0].split("=")[0] == "class_id"):
            #print(fields)
            class_name = fields[1].split("=")[1]
            ap = fields[2].split("(")[0].split("=")[1].split("%")[0]
            tp = fields[2].split("(")[1].split("=")[1]
            fp = fields[3].split("=")[1]
            fn = fields[4].split("=")[1]
            iou = fields[6].split("=")[1].split(")")[0]
            result_string = "%s\n %s & %s & %s & %s & %s & %s & " % (result_string, class_name, ap, tp, fp, fn, iou)
        elif(fields[0].split(",")[0].split("=")[0] == "Precision"):
            precision = fields[0].split("=")[1]
            recall = fields[1].split("=")[1]
            result_string = "%s%s & %s \\\\" % (result_string, precision, recall)
            print("Precision and recall: " + precision + " " + recall)
        else:
            print("Ignoring string line " + fields[0])

result_string = "%s\n\\end{tabular}\n\\end{center}\n\\caption{}\n\\end{table}" % result_string
print(result_string)
